<?php

/**
 * Distance Calculator using Google's Distance Matrix.
 * @param string $origin 
 * @param string $destination
 * @return boolean|string
 * @author Jonathan MacGregor
 */

function _process_distance($origin, $destination) {
	// Remove any whitespace
	$origin = str_replace(' ','',$origin);
	$destination = str_replace(' ','', $destination);
	
	// Build JSON URL
	$jsonurl = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&mode=driving&sensor=false&units=imperial";
	
	// Fetch response
	$json = file_get_contents($jsonurl,0,null,null);
	
	// Decode JSON response so we can use it
	$json_output = json_decode($json);

	// If no response, return false
	if($json_output->rows[0]->elements[0]->status == 'NOT_FOUND') {
		return false;
	}
	
	// There is a response..
	// Grab the distance in meters
	if(isset($json_output->rows[0]->elements[0]->distance))
		$meters = $json_output->rows[0]->elements[0]->distance->value;
	else
		return false;
	
	// Convert into miles.
	$miles = number_format($meters / 1612, 2);
	
	// $miles = 51;
	return $miles;
}
